package dolphin

// HTTP header names.
const (
	// HeaderContentType is the "Content-Type" header.
	HeaderContentType = "Content-Type"
	// Host is the "Host" header.
	HeaderHost = "Host"
	// HeaderLocation is the "Location" header.
	HeaderLocation = "Location"
	// HeaderReferrer is the "Referer" header.
	HeaderReferrer = "Referrer"
	// HeaderSetCookie is the "Set-Cookie" header.
	HeaderSetCookie = "Set-Cookie"
)

// MIME types for HTTP content type.
const (
	// MIMETypeJSON indicates the "application/json" MIME type.
	MIMETypeJSON = "application/json"
	// MIMETypeHTML indicates the "text/html" MIME type.
	MIMETypeHTML = "text/html"
	// MIMETypeText indicates the "text/plain" MIME type.
	MIMETypeText = "text/plain"
)
